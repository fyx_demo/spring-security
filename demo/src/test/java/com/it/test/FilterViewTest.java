package com.it.test;

import com.it.filter.JwtAuthFilter;
import org.junit.jupiter.api.Test;

/**
 * @see org.springframework.security.web.FilterChainProxy
 * Spring Security启动时默认的过滤器的执行顺序如下：
 *
 * @see org.springframework.security.web.session.DisableEncodeUrlFilter
 * @see org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter
 * @see org.springframework.security.web.context.SecurityContextHolderFilter
 * @see org.springframework.security.web.header.HeaderWriterFilter
 * @see org.springframework.web.filter.CorsFilter
 * @see org.springframework.security.web.authentication.logout.LogoutFilter
 * @see JwtAuthFilter
 * @see org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
 * @see org.springframework.security.web.authentication.ui.DefaultLoginPageGeneratingFilter
 * @see org.springframework.security.web.authentication.ui.DefaultLogoutPageGeneratingFilter
 * @see org.springframework.security.web.savedrequest.RequestCacheAwareFilter
 * @see org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter
 * @see org.springframework.security.web.authentication.AnonymousAuthenticationFilter
 * @see org.springframework.security.web.session.SessionManagementFilter
 * @see org.springframework.security.web.access.ExceptionTranslationFilter
 * @see org.springframework.security.web.access.intercept.AuthorizationFilter
 */
public class FilterViewTest {

	@Test
	void filter1() {

	}
}
