package com.it.controller;

import com.it.utils.ResultUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

	@GetMapping("/hello")
	public ResultUtil<?> hello() {
		return ResultUtil.result("Hello World!");
	}
}
