package com.it.filter;

import com.it.mapper.UserMapper;
import com.it.utils.JwtUtils;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class JwtAuthFilter extends OncePerRequestFilter {

	private static final String prefix = "Bearer ";

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private JwtUtils jwtUtils;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		// 获取 HTTP 请求头部中的 Authorization 字段
		final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		final String userEmail;
		final String jwtToken;

		// 如果 Authorization 字段不存在或者不符合 Bearer Token 的格式，则跳过该过滤器
		if (authHeader == null || !authHeader.startsWith(prefix)) {
			filterChain.doFilter(request, response);
			return;
		}

		// 提取 JWT Token，并从中获取用户邮箱
		jwtToken = authHeader.substring(prefix.length());
		userEmail = jwtUtils.extractUsername(jwtToken);

		// 如果用户邮箱不为空且未进行身份验证
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (userEmail != null && authentication == null) {
			// 根据用户邮箱从 UserDao 中查找用户
			UserDetails userDetails = userMapper.findUserByEmail(userEmail);

			// 如果 JWT Token 有效，则进行身份验证
			if (jwtUtils.isTokenValid(jwtToken, userDetails)) {
				UsernamePasswordAuthenticationToken authToken =
						new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authToken);
			}
		}

		// 继续处理请求
		filterChain.doFilter(request, response);
	}
}
