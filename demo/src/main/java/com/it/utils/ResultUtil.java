package com.it.utils;

import java.io.Serializable;

public final class ResultUtil<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code = 0;

    private String message = "success";

    private T data = null;

    public ResultUtil() {
    }

    public ResultUtil(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResultUtil(String message, T data) {
        this.message = message;
        this.data = data;
    }

    public ResultUtil(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResultUtil(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> ResultUtil<T> success() {
        return new ResultUtil<>();
    }

    public static <T> ResultUtil<T> success(String message) {
        return new ResultUtil<>(message);
    }

    public static <T> ResultUtil<T> success(String message, T data) {
        return new ResultUtil<>(message, data);
    }

    public static <T> ResultUtil<T> error(String message) {
        return new ResultUtil<>(500, message);
    }

    public static <T> ResultUtil<T> error(Integer code, String message) {
        return new ResultUtil<>(code, message);
    }

    public static <T> ResultUtil<T> result(String message) {
        return new ResultUtil<>(message);
    }

    public static <T> ResultUtil<T> result(Integer code, String message) {
        return new ResultUtil<>(code, message);
    }

    public static <T> ResultUtil<T> result(Integer code, String message, T data) {
        return new ResultUtil<>(code, message, data);
    }

    @Override
    public String toString() {
        return "ResultUtil{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
