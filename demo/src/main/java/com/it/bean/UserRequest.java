package com.it.bean;

import lombok.Data;

@Data
public class UserRequest {

    private String email;

    private String password;

}
